<?php

namespace Soluti\DataFilterBundle\Transformer;

use Traversable;

abstract class AbstractTransformer implements TransformerInterface
{
    /**
     * @param array|Traversable $data
     * @return array
     */
    public function transformCollection($data)
    {
        $result = [];
        foreach ($data as $item) {
            $result[] = $this->transform($item);
        }

        return $result;
    }

    /**
     * @param mixed $data
     * @return array
     */
    abstract public function transform($data);
}
