<?php

namespace Soluti\DataFilterBundle\Transformer;

use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\ResourceAbstract;
use League\Fractal\TransformerAbstract;
use Soluti\DataFilterBundle\Manager\FractalManager;
use Traversable;

abstract class ApiAbstractTransformer extends TransformerAbstract implements TransformerInterface
{
    /** @var FractalManager */
    protected $fractalManager;

    /**
     * @param FractalManager $fractalManager
     */
    public function __construct(FractalManager $fractalManager)
    {
        $this->fractalManager = $fractalManager;
    }

    /**
     * @param array|Traversable $data
     * @return array
     */
    public function transformCollection($data)
    {
        return $this->fractalResourceToArray(new FractalCollection($data, $this));
    }

    protected function fractalResourceToArray(ResourceAbstract $resource)
    {
        return $this->fractalManager->createData($resource)->toArray();
    }

    /**
     * @param mixed $data
     * @return array
     */
    abstract public function transform($data);
}
