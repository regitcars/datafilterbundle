<?php

namespace Soluti\DataFilterBundle\Transformer;

use Traversable;

interface TransformerInterface
{
    /**
     * @param array|Traversable $data
     *
     * @return array
     */
    public function transformCollection($data);

    /**
     * @param mixed $data
     *
     * @return array
     */
    public function transform($data);
}
