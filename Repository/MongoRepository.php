<?php

namespace Soluti\DataFilterBundle\Repository;

use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Soluti\DataFilterBundle\Definition\DoctrineODMFilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Filter\FilterResult;
use Doctrine\ODM\MongoDB\Query\Builder;

abstract class MongoRepository extends ServiceDocumentRepository implements FilterableRepositoryInterface
{
    /**
     * @param CollectionFilter $filter
     *
     * @return FilterResult
     */
    public function findFiltered(CollectionFilter $filter)
    {
        return new FilterResult(
            $filter,
            $this->getTotalCount($filter),
            $this->getFilteredCount($filter),
            $this->getData($filter)
        );
    }

    /**
     * @param CollectionFilter $filter
     * @return int
     */
    public function getTotalCount(CollectionFilter $filter)
    {
        return $this->getCount(
            $this->getFilterQueryBuilder($filter),
            $filter->getPredefinedCriteria()
        );
    }

    /**
     * @param Builder $qb
     * @param array $criteria
     * @return int
     */
    protected function getCount(Builder $qb, array $criteria)
    {
        foreach ($criteria as $columnName => $filter) {
            $qb->field($columnName)->equals($filter);
        }

        return $qb->count()->getQuery()->execute();
    }

    /**
     * @param CollectionFilter $filter
     * @return Builder
     */
    protected function getFilterQueryBuilder(CollectionFilter $filter)
    {
        $filterDefinition = $filter->getDefinition();

        if ($filterDefinition instanceof DoctrineODMFilterDefinitionInterface) {
            return $filterDefinition->getQueryBuilder($this);
        }

        return $this->createQueryBuilder();
    }

    /**
     * @param CollectionFilter $filter
     * @return int
     */
    public function getFilteredCount(CollectionFilter $filter)
    {
        return $this->getCount(
            $this->getFilterQueryBuilder($filter),
            $filter->getCriteria()
        );
    }

    /**
     * @param CollectionFilter $filter
     * @return Traversable
     */
    protected function getData(CollectionFilter $filter)
    {
        $qb = $this->getFilterQueryBuilder($filter);

        foreach ($filter->getSortOrder() as $sortKey => $sortOrder) {
            $qb->sort($sortKey, $sortOrder);
        }

        // filtering
        foreach ($filter->getCriteria() as $columnName => $criteria) {
            $qb->field($columnName)->equals($criteria);
        }

        // pagination
        $qb->skip($filter->getOffset());
        $qb->limit($filter->getLimit());

        return $qb->getQuery()->execute();
    }
}
