<?php

namespace Soluti\DataFilterBundle\Exception;

class InvalidSortDefinitionException extends \Exception
{
}
