<?php

namespace Soluti\DataFilterBundle\Sort;

use Soluti\DataFilterBundle\Exception\InvalidSortDefinitionException;

class SortConfiguration
{
    /** @var array|SortDefinition[] */
    private $sortDefinitions;

    /** @var array|SortDefinition[] */
    private $sortDefinitionsByName;

    /** @var array|SortDefinition[] */
    private $sortDefinitionsByIndex;

    /**
     * @param array $sortDefinitions
     * @throws \Exception
     */
    public function __construct(array $sortDefinitions = [])
    {
        foreach ($sortDefinitions as $sortDefinition) {
            if (!$sortDefinition instanceof SortDefinition) {
                throw new InvalidSortDefinitionException('Provided sort definition is not a SortDefinition');
            }
        }

        $this->sortDefinitions = $sortDefinitions;
    }

    /**
     * @param string $name
     * @return null|SortDefinition
     */
    public function getSortDefinitionByName(string $name): ?SortDefinition
    {
        if (null === $this->sortDefinitionsByName) {
            $this->initNameIndex();
        }

        return $this->sortDefinitionsByName[$name] ?? null;
    }

    /**
     * @throws \Exception
     */
    private function initNameIndex(): void
    {
        if (null === $this->sortDefinitionsByName) {
            $this->sortDefinitionsByName = [];
        }
        foreach ($this->sortDefinitions as $sortDefinition) {
            if (array_key_exists(
                $sortDefinition->getName(),
                $this->sortDefinitionsByName
            )) {
                throw new InvalidSortDefinitionException('SortDefinition with same name defined twice');
            }

            $this->sortDefinitionsByName[$sortDefinition->getName()] = $sortDefinition;
        }
    }

    /**
     * @param int $index
     * @return null|SortDefinition
     */
    public function getSortDefinitionByIndex(int $index): ?SortDefinition
    {
        if (null === $this->sortDefinitionsByIndex) {
            $this->initNumericIndex();
        }

        return $this->sortDefinitionsByIndex[$index] ?? null;
    }

    /**
     * @throws \Exception
     */
    private function initNumericIndex(): void
    {
        foreach ($this->sortDefinitions as $sortDefinition) {
            if (!is_int($sortDefinition->getIndex())) {
                throw new InvalidSortDefinitionException('SortDefinition does not have numeric index');
            }

            if (isset($this->sortDefinitionsByIndex[$sortDefinition->getIndex()])) {
                throw new InvalidSortDefinitionException('SortDefinition with same index defined twice');
            }

            $this->sortDefinitionsByIndex[$sortDefinition->getIndex()] = $sortDefinition;
        }
    }

    /**
     * @return array|SortDefinition[]
     */
    public function getAllDefinitions(): array
    {
        return $this->sortDefinitions;
    }
}
