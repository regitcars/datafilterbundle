<?php

namespace Soluti\DataFilterBundle\Sort;

use Soluti\DataFilterBundle\Exception\InvalidValueException;

class SortDefinition
{
    const SORT_ASC = 'asc';
    const SORT_DESC = 'desc';

    /** @var string */
    private $name;

    /** @var string */
    private $columnName;

    /** @var int|null */
    private $index;

    /** @var null|string */
    private $defaultSortOrder;

    /**
     * @param string $name
     * @param string|null $columnName
     * @param int|null $index
     * @param string|null $defaultSortOrder
     */
    public function __construct(
        string $name,
        string $columnName = null,
        int $index = null,
        string $defaultSortOrder = null
    ) {
        if (null === $columnName) {
            $columnName = $name;
        }

        if (null !== $defaultSortOrder) {
            $this->validateSortOrder($defaultSortOrder);
        }

        $this->name = $name;
        $this->columnName = $columnName;
        $this->index = $index;
        $this->defaultSortOrder = $defaultSortOrder;
    }

    /**
     * @param string $sortOrder
     * @throws \Exception
     */
    protected function validateSortOrder(string $sortOrder): void
    {
        if (!in_array($sortOrder, [self::SORT_ASC, self::SORT_DESC])) {
            throw new InvalidValueException('Unsupported sort direction.');
        }
    }

    /**
     * @param string $sortOrder
     * @return array
     * @throws \Exception
     */
    public function getSortOrder(string $sortOrder): array
    {
        $this->validateSortOrder($sortOrder);

        return [
            $this->columnName => $sortOrder,
        ];
    }

    public function getDefaultSortOrder(): array
    {
        if (null === $this->defaultSortOrder) {
            return [];
        }

        return [
            $this->columnName => $this->defaultSortOrder,
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getIndex(): ?int
    {
        return $this->index;
    }
}
