<?php

namespace Soluti\DataFilterBundle\Formatter;

use Soluti\DataFilterBundle\Filter\FilterResult;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;

interface FormatterInterface
{
    /**
     * @param FilterResult $result
     * @param TransformerInterface $transformer
     * @return array
     */
    public function format(FilterResult $result, TransformerInterface $transformer);
}
