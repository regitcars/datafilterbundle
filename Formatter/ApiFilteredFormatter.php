<?php

namespace Soluti\DataFilterBundle\Formatter;

use Soluti\DataFilterBundle\Filter\FilterResult;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;

class ApiFilteredFormatter implements FormatterInterface
{
    /**
     * @param FilterResult $result
     * @param TransformerInterface $transformer
     * @return array
     */
    public function format(FilterResult $result, TransformerInterface $transformer)
    {
        $pagination = [];

        if ($result->getFilter() &&
            $result->getFilter()->getDefinition() &&
            $result->getFilter()->getDefinition()->getPaginationConfiguration()
        ) {
            $pagination = [
                'pagination' => [
                    'total' => $result->getFilteredResults(),
                    'page' => round($result->getFilter()->getOffset() / $result->getFilter()->getLimit()) + 1,
                    'per_page' => $result->getFilter()->getLimit(),
                ],
            ];
        }

        return array_merge(
            $pagination,
            $transformer->transformCollection($result->getData())
        );
    }
}
