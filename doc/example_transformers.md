## Example Transformers

Transformers are registered as a service in order to be injected inside the Filter Definition. 
You can use any extra dependencies in your constructor.

```yml
  AppBundle\Filter\Transformer\UserTransformer:
    class: AppBundle\Filter\Transformer\UserTransformer
```

Hint: you can use **auto wiring** and automatically register all your transformers as service.

#### Api:

```php
namespace AppBundle\Filter\Transformer;

use AppBundle\Model\User;
use Soluti\DataFilterBundle\Transformer\AbstractTransformer;

class UserTransformer extends ApiAbstractTransformer
{
    /**
     * @param User $data
     *
     * @return array
     */
    public function transform($data)
    {
        return [
            'id' => $data->getId(),
            'first_name' => $data->getFirstName(),
            'last_name' => $data->getLastName(),
        ];
    }
}
```

#### DataTables:

```php
namespace AppBundle\Filter\Transformer;

use AppBundle\Model\User;
use Soluti\DataFilterBundle\Transformer\AbstractTransformer;

class UserTransformer extends AbstractTransformer
{
    /**
     * @param User $data
     *
     * @return array
     */
    public function transform($data)
    {
        return [
            'id' => $data->getId(),
            'first_name' => $data->getFirstName(),
            'last_name' => $data->getLastName(),
        ];
    }
}
```
