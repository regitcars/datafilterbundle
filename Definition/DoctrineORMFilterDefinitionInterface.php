<?php

namespace Soluti\DataFilterBundle\Definition;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

interface DoctrineORMFilterDefinitionInterface extends FilterDefinitionInterface
{
    public function getQueryBuilder(EntityRepository $repository): QueryBuilder;
}
