<?php

namespace Soluti\DataFilterBundle\Definition;

use Soluti\DataFilterBundle\Filter\FilterConfiguration;
use Soluti\DataFilterBundle\Pagination\PaginationConfiguration;
use Soluti\DataFilterBundle\Repository\FilterableRepositoryInterface;
use Soluti\DataFilterBundle\Sort\SortConfiguration;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class BaseFilterDefinition implements FilterDefinitionInterface
{
    /** @var FilterableRepositoryInterface */
    private $repository;

    /** @var TransformerInterface */
    private $transformer;

    /**
     * @param FilterableRepositoryInterface $repository
     * @param TransformerInterface $transformer
     */
    public function __construct(FilterableRepositoryInterface $repository, TransformerInterface $transformer)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
    }

    public function getPaginationConfiguration(): ?PaginationConfiguration
    {
        return new PaginationConfiguration();
    }

    public function getSortConfiguration(): SortConfiguration
    {
        return new SortConfiguration();
    }

    /**
     * @return FilterConfiguration
     */
    public function getFilterConfiguration(): FilterConfiguration
    {
        return new FilterConfiguration();
    }

    /**
     * @param Request $request
     * @return FilterConfiguration
     */
    public function getDefaultFilterConfiguration(Request $request): FilterConfiguration
    {
        return new FilterConfiguration();
    }

    /**
     * @param Request $request
     * @return FilterConfiguration
     */
    public function getPredefinedFilterConfiguration(Request $request): FilterConfiguration
    {
        return new FilterConfiguration();
    }

    public function getRepositoryService(): FilterableRepositoryInterface
    {
        return $this->repository;
    }

    public function getTransformerService(): TransformerInterface
    {
        return $this->transformer;
    }
}
