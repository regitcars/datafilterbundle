<?php

namespace Soluti\DataFilterBundle\Definition;

use Soluti\DataFilterBundle\Filter\FilterConfiguration;
use Soluti\DataFilterBundle\Pagination\PaginationConfiguration;
use Soluti\DataFilterBundle\Repository\FilterableRepositoryInterface;
use Soluti\DataFilterBundle\Sort\SortConfiguration;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;
use Symfony\Component\HttpFoundation\Request;

interface FilterDefinitionInterface
{
    public function getPaginationConfiguration(): ?PaginationConfiguration;

    public function getFilterConfiguration(): FilterConfiguration;

    public function getSortConfiguration(): SortConfiguration;

    public function getDefaultFilterConfiguration(Request $request): FilterConfiguration;

    public function getPredefinedFilterConfiguration(Request $request): FilterConfiguration;

    public function getRepositoryService(): FilterableRepositoryInterface;

    public function getTransformerService(): TransformerInterface;
}
