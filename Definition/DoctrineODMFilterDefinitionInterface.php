<?php

namespace Soluti\DataFilterBundle\Definition;

use Doctrine\ODM\MongoDB\Repository\DocumentRepository;
use Doctrine\ODM\MongoDB\Query\Builder;

interface DoctrineODMFilterDefinitionInterface extends FilterDefinitionInterface
{
    public function getQueryBuilder(DocumentRepository $repository): Builder;
}
