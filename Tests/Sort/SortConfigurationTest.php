<?php

namespace Soluti\DataFilterBundle\Tests\Sort;

use Soluti\DataFilterBundle\Exception\InvalidSortDefinitionException;
use Soluti\DataFilterBundle\Sort\SortConfiguration;
use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\Sort\SortDefinition;
use Symfony\Component\HttpFoundation\Request;

class SortConfigurationTest extends TestCase
{
    public function testInvalidFilter()
    {
        $this->expectException(InvalidSortDefinitionException::class);
        new SortConfiguration(
            [
                new Request(),
            ]
        );
    }

    public function testDuplicateName()
    {
        $this->expectException(InvalidSortDefinitionException::class);
        $sortConfiguration = new SortConfiguration(
            [
                new SortDefinition('name', 'a.alias', 1, SortDefinition::SORT_ASC),
                new SortDefinition('name', 'a.alias', 2, SortDefinition::SORT_ASC),
            ]
        );

        $sortConfiguration->getSortDefinitionByName('name');
    }

    public function testDuplicateIndexFilter()
    {
        $this->expectException(InvalidSortDefinitionException::class);
        $sortConfiguration = new SortConfiguration(
            [
                new SortDefinition('name', 'a.alias', 1, SortDefinition::SORT_ASC),
                new SortDefinition('email', 'a.alias', 1, SortDefinition::SORT_ASC),
            ]
        );

        $sortConfiguration->getSortDefinitionByIndex(1);
    }

    public function testNoIndexFilter()
    {
        $this->expectException(InvalidSortDefinitionException::class);
        $sortConfiguration = new SortConfiguration(
            [
                new SortDefinition('name', 'a.alias', null, SortDefinition::SORT_ASC),
            ]
        );

        $sortConfiguration->getSortDefinitionByIndex(1);
    }

    public function testGetFilterByName()
    {
        $sortConfiguration = $this->getSortConfiguration();
        $this->assertInstanceOf(SortDefinition::class, $sortConfiguration->getSortDefinitionByName('email'));
        $this->assertNull($sortConfiguration->getSortDefinitionByName('test'));
    }

    public function testGetFilterByIndex()
    {
        $sortConfiguration = $this->getSortConfiguration();
        $this->assertInstanceOf(SortDefinition::class, $sortConfiguration->getSortDefinitionByIndex(5));
        $this->assertNull($sortConfiguration->getSortDefinitionByName(99));
    }

    public function testGetAllFilters()
    {
        $sortConfiguration = $this->getSortConfiguration();
        $this->assertCount(2, $sortConfiguration->getAllDefinitions());
    }

    protected function getSortConfiguration()
    {
        return new SortConfiguration(
            [
                new SortDefinition('name', 'a.alias', 1, SortDefinition::SORT_ASC),
                new SortDefinition('email', 'a.alias', 5, SortDefinition::SORT_DESC),
            ]
        );
    }
}
