<?php

namespace Soluti\DataFilterBundle\Tests\Sort;

use Soluti\DataFilterBundle\Exception\InvalidValueException;
use Soluti\DataFilterBundle\Sort\SortDefinition;
use PHPUnit\Framework\TestCase;

class SortDefinitionTest extends TestCase
{
    public function testSortDefinition()
    {
        $sortDefinition = new SortDefinition('name', 'a.alias', 1, SortDefinition::SORT_ASC);
        $this->assertEquals('name', $sortDefinition->getName());
        $this->assertEquals(1, $sortDefinition->getIndex());
        $this->assertEquals(['a.alias' => 'asc'], $sortDefinition->getDefaultSortOrder());
        $this->assertEquals(['a.alias' => 'desc'], $sortDefinition->getSortOrder('desc'));
    }

    public function testSortDefinitionNoDefault()
    {
        $sortDefinition = new SortDefinition('name', 'a.alias', 1);
        $this->assertEquals([], $sortDefinition->getDefaultSortOrder());
    }

    public function testSortDefinitionInvalidSort()
    {
        $this->expectException(InvalidValueException::class);
        $sortDefinition = new SortDefinition('name', 'a.alias', 1);
        $sortDefinition->getSortOrder(1);
    }

    public function testSortDefinitionNoColumnName()
    {
        $sortDefinition = new SortDefinition('name');
        $this->assertEquals(['name' => 'desc'], $sortDefinition->getSortOrder('desc'));
    }
}
