<?php

namespace Soluti\DataFilterBundle\Tests\Manager;

use League\Fractal\Resource\Collection as FractalCollection;
use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\Manager\FractalManager;
use Soluti\DataFilterBundle\Transformer\ApiAbstractTransformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class FractalManagerTest extends TestCase
{
    public function testCreateData()
    {
        $fractalManager = new FractalManager($this->getRequestStack());
        $result = $fractalManager->createData(
            new FractalCollection(
                [
                    $this->getObject(1),
                ],
                $this->getTransformer($fractalManager)
            )
        )->toArray();

        $this->assertEquals(
            [
                'data' => [
                    [
                        'first_name' => 'FirstName1',
                        'geo' => [
                            'data' => [
                                'lat' => 10,
                                'lng' => -5,
                            ],
                        ],
                    ],
                ],
            ],
            $result
        );
    }

    private function getRequestStack()
    {
        $requestStack = new RequestStack();
        $requestStack->push(
            new Request(
                [
                    'include' => 'geo',
                    'exclude' => 'address',
                ]
            )
        );

        return $requestStack;
    }

    protected function getObject($i)
    {
        $address = $this->getAddressObject($i);
        $geo = $this->getGeoObject($i);

        return new class($i, $address, $geo)
        {
            protected $firstName;

            protected $address;

            protected $geo;

            public function __construct($i, $address, $geo)
            {
                $this->firstName = 'FirstName'.$i;
                $this->address = $address;
                $this->geo = $geo;
            }

            public function getFirstName()
            {
                return $this->firstName;
            }

            public function getAddress()
            {
                return $this->address;
            }

            public function getGeo()
            {
                return $this->geo;
            }
        };
    }

    private function getAddressObject($i)
    {
        return new class($i)
        {
            protected $street;

            protected $city;

            public function __construct($i)
            {
                $this->street = 'Street'.$i;
                $this->city = 'City'.$i;
            }

            public function getStreet()
            {
                return $this->street;
            }

            public function getCity()
            {
                return $this->city;
            }
        };
    }

    private function getGeoObject($i)
    {
        return new class($i)
        {
            protected $lat;

            protected $lng;

            public function __construct($i)
            {
                $this->lng = $i * -5;
                $this->lat = $i * 10;
            }

            public function getLng()
            {
                return $this->lng;
            }

            public function getLat()
            {
                return $this->lat;
            }
        };
    }

    private function getTransformer($fractalManager)
    {
        return new class($fractalManager) extends ApiAbstractTransformer
        {

            protected $defaultIncludes = [
                'address',
            ];

            protected $availableIncludes = [
                'geo',
            ];

            public function transform($data)
            {
                return [
                    'first_name' => $data->getFirstName(),
                ];
            }

            public function includeAddress($data)
            {
                return $this->item(
                    $data->getAddress(),
                    function ($address) {
                        return [
                            'street' => $address->getStreet(),
                            'city' => $address->getCity(),
                        ];
                    }
                );
            }

            public function includeGeo($data)
            {
                return $this->item(
                    $data->getGeo(),
                    function ($geo) {
                        return [
                            'lat' => $geo->getLat(),
                            'lng' => $geo->getLng(),
                        ];
                    }
                );
            }
        };
    }
}
