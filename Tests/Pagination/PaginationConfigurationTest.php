<?php

namespace Soluti\DataFilterBundle\Tests\Pagination;

use Soluti\DataFilterBundle\Pagination\PaginationConfiguration;
use PHPUnit\Framework\TestCase;

class PaginationConfigurationTest extends TestCase
{
    /**
     * @dataProvider dataPage
     * @param $page
     * @param $limit
     * @param $result
     */
    public function testGetByPage($page, $limit, $result)
    {
        $pagination = new PaginationConfiguration(5, 20);
        $this->assertEquals($result, $pagination->getByPage($page, $limit));
    }

    public function dataPage()
    {
        return [
            [5, 0, [20,5]],
            [-1, 0, [0,5]],
            [1, 10, [0,10]],
            [1, 100, [0,20]],
        ];
    }

    /**
     * @dataProvider dataOffset
     * @param $offset
     * @param $limit
     * @param $result
     */
    public function testGetByOffset($offset, $limit, $result)
    {
        $pagination = new PaginationConfiguration(5, 20);
        $this->assertEquals($result, $pagination->getByOffset($offset, $limit));
    }

    public function dataOffset()
    {
        return [
            [5, 0, [5,5]],
            [-1, 0, [0,5]],
            [0, 10, [0,10]],
            [50, 100, [50,20]],
        ];
    }
}
