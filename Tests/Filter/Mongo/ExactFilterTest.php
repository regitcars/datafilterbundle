<?php

namespace Soluti\DataFilterBundle\Tests\Filter\Mongo;

use Soluti\DataFilterBundle\DataType\StringDataType;
use Soluti\DataFilterBundle\Filter\Mongo\ExactFilter;
use Soluti\DataFilterBundle\Tests\Filter\BaseFilterTest;

class ExactFilterTest extends BaseFilterTest
{
    /**
     * @dataProvider getFilterOptions
     *
     * @param $value
     * @param $result
     */
    public function testGetFilter($value, $result)
    {
        $filter = new ExactFilter(...$this->getFilterArguments());
        $filterResult = $filter->getFilter($value);
        $this->assertEquals($result, $filterResult);
    }

    public function getFilterOptions()
    {
        return [
            [null, []],
            ['', []],
            [['test'=>''], []],
            [
                'some_value',
                [
                    'a.alias' => 'some_value'
                ]
            ],
        ];
    }

    protected function getDataType($value)
    {
        return StringDataType::class;
    }
}
