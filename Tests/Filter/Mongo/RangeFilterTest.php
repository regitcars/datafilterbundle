<?php

namespace Soluti\DataFilterBundle\Tests\Filter\Mongo;

use MongoDB\BSON\UTCDateTime;
use Soluti\DataFilterBundle\DataType\MongoDateTimeDataType;
use Soluti\DataFilterBundle\Filter\Mongo\RangeFilter;
use Soluti\DataFilterBundle\Tests\Filter\BaseFilterTest;

class RangeFilterTest extends BaseFilterTest
{
    /**
     * @dataProvider getFilterOptions
     *
     * @param $value
     * @param $result
     */
    public function testGetFilter($value, $result)
    {
        $filter = new RangeFilter(...$this->getFilterArguments());
        $filterResult = $filter->getFilter($value);
        $this->assertEquals($result, $filterResult);
    }

    public function getFilterOptions()
    {
        return [
            [null, []],
            ['', []],
            [['test'=>''], []],
            [
                [
                    'start' => '01-01-2017',
                    'end' => '30-01-2017',
                ],
                [
                    'a.alias' => [
                        '$gte' => new UTCDateTime('1483228800000'),
                        '$lte' => new UTCDateTime('1485734400000'),
                    ]
                ]
            ],
        ];
    }

    protected function getDataType($value)
    {
        return MongoDateTimeDataType::class;
    }
}
