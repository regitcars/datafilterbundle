<?php

namespace Soluti\DataFilterBundle\Tests\Filter;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\DataType\DataTypeInterface;

abstract class BaseFilterTest extends TestCase
{
    protected function getFilterArguments($value = 'mockValue')
    {
        return [
            'name',
            $this->getDataType($value),
            'a.alias',
            [
                'index' => 5,
            ],
            'default',
        ];
    }

    /**
     * @param string $value
     * @return \PHPUnit_Framework_MockObject_MockObject|DataTypeInterface
     */
    protected function getDataType($value)
    {
        $stub = $this->createMock(DataTypeInterface::class);

        $stub
            ->method('prepare')
            ->willReturn($value);

        return $stub;
    }
}
