<?php
namespace Soluti\DataFilterBundle\Tests\Filter;

use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use PHPUnit\Framework\TestCase;

class CollectionFilterTest extends TestCase
{
    public function testSettersGetters()
    {
        /** @var FilterDefinitionInterface $filterDefinition */
        $filterDefinition = $this->createMock(FilterDefinitionInterface::class);
        $collectionFilter = new CollectionFilter($filterDefinition);
        $collectionFilter->setCriteria(['a']);
        $collectionFilter->setPredefinedCriteria(['b']);
        $collectionFilter->setSortOrder(['c']);
        $collectionFilter->setLimit(55);
        $collectionFilter->setOffset(44);

        $this->assertEquals(['a'], $collectionFilter->getCriteria());
        $this->assertEquals(['b'], $collectionFilter->getPredefinedCriteria());
        $this->assertEquals(['c'], $collectionFilter->getSortOrder());
        $this->assertEquals(55, $collectionFilter->getLimit());
        $this->assertEquals(44, $collectionFilter->getOffset());
        $this->assertEquals($filterDefinition, $collectionFilter->getDefinition());
    }
}
