<?php

namespace Soluti\DataFilterBundle\Tests\Filter\Doctrine;

use Soluti\DataFilterBundle\DataType\LikeStringDataType;
use Soluti\DataFilterBundle\Filter\Doctrine\LikeFilter;
use Soluti\DataFilterBundle\Tests\Filter\BaseFilterTest;

class LikeFilterTest extends BaseFilterTest
{
    /**
     * @dataProvider getFilterOptions
     *
     * @param $value
     * @param $result
     */
    public function testGetFilter($value, $result)
    {
        $filter = new LikeFilter(...$this->getFilterArguments());
        $filterResult = $filter->getFilter($value);
        $this->assertEquals($result, $filterResult);
    }

    public function getFilterOptions()
    {
        return [
            [null, []],
            ['', []],
            [['test'=>''], []],
            [
                'some_value',
                [
                    'name' => [
                        'statement' => "a.alias LIKE (:name) ESCAPE '!'",
                        'parameters' => ['name' => 'some!_value%']
                    ]
                ]
            ],
        ];
    }

    protected function getDataType($value)
    {
        return LikeStringDataType::class;
    }
}
