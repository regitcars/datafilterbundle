<?php

namespace Soluti\DataFilterBundle\Tests\Filter\Doctrine;

use Soluti\DataFilterBundle\DataType\ArrayDataType;
use Soluti\DataFilterBundle\Exception\InvalidValueException;
use Soluti\DataFilterBundle\Filter\Doctrine\InArrayFilter;
use Soluti\DataFilterBundle\Tests\Filter\BaseFilterTest;

class InArrayFilterTest extends BaseFilterTest
{
    /**
     * @dataProvider getFilterOptions
     *
     * @param $value
     * @param $result
     */
    public function testGetFilter($value, $result)
    {
        $filter = new InArrayFilter(...$this->getFilterArguments());
        $filterResult = $filter->getFilter($value);
        $this->assertEquals($result, $filterResult);
    }

    public function testGetFilterAllowedValues()
    {
        $filterArguments = $this->getFilterArguments();
        $filterArguments[3] = ['allowedValues' => [1, 2, 3]];
        $filter = new InArrayFilter(...$filterArguments);
        $filterResult = $filter->getFilter([1]);
        $this->assertEquals(
            [
                'name' => [
                    'statement' => 'a.alias IN (:name)',
                    'parameters' => ['name' => [1]],
                ],
            ],
            $filterResult
        );
    }

    public function testGetFilterInvalidAllowedValues()
    {
        $this->expectException(InvalidValueException::class);
        $filterArguments = $this->getFilterArguments();
        $filterArguments[3] = ['allowedValues' => [1, 2, 3]];
        $filter = new InArrayFilter(...$filterArguments);
        $filter->getFilter(5);
    }

    public function getFilterOptions()
    {
        return [
            [null, []],
            ['', []],
            [['test' => ''], []],
            [
                [1, 2, 3],
                [
                    'name' => [
                        'statement' => 'a.alias IN (:name)',
                        'parameters' => ['name' => [1, 2, 3]],
                    ],
                ],
            ],
        ];
    }


    protected function getDataType($value)
    {
        return ArrayDataType::class;
    }
}
