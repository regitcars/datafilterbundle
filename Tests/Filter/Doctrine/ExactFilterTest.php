<?php

namespace Soluti\DataFilterBundle\Tests\Filter\Doctrine;

use Soluti\DataFilterBundle\DataType\StringDataType;
use Soluti\DataFilterBundle\Filter\Doctrine\ExactFilter;
use Soluti\DataFilterBundle\Tests\Filter\BaseFilterTest;

class ExactFilterTest extends BaseFilterTest
{
    /**
     * @dataProvider getFilterOptions
     *
     * @param $value
     * @param $result
     */
    public function testGetFilter($value, $result)
    {
        $filter = new ExactFilter(...$this->getFilterArguments());
        $filterResult = $filter->getFilter($value);
        $this->assertEquals($result, $filterResult);
    }

    public function getFilterOptions()
    {
        return [
            [null, []],
            ['', []],
            [['test'=>''], []],
            [
                'some_value',
                [
                    'name' => [
                        'statement' => 'a.alias = :name',
                        'parameters' => ['name' => 'some_value']
                    ]
                ]
            ],
        ];
    }

    protected function getDataType($value)
    {
        return StringDataType::class;
    }
}
