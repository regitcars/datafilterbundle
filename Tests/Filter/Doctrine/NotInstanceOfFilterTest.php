<?php

namespace Soluti\DataFilterBundle\Tests\Filter\Doctrine;

use Soluti\DataFilterBundle\DataType\StringDataType;
use Soluti\DataFilterBundle\Exception\InvalidValueException;
use Soluti\DataFilterBundle\Filter\Doctrine\NotInstanceOfFilter;
use Soluti\DataFilterBundle\Tests\Filter\BaseFilterTest;

class NotInstanceOfFilterTest extends BaseFilterTest
{
    /**
     * @dataProvider getFilterOptions
     *
     * @param $value
     * @param $result
     */
    public function testGetFilter($value, $result)
    {
        $filter = new NotInstanceOfFilter(...$this->getFilterArguments());
        $filterResult = $filter->getFilter($value);
        $this->assertEquals($result, $filterResult);
    }

    public function testGetFilterInvalid()
    {
        $this->expectException(InvalidValueException::class);
        $filter = new NotInstanceOfFilter(...$this->getFilterArguments());
        $filter->getFilter('some_value');
    }

    public function getFilterOptions()
    {
        return [
            [null, []],
            ['', []],
            [['test'=>''], []],
            [
                self::class,
                [
                    'name' => [
                        'statement' => 'a.alias NOT INSTANCE OF Soluti\DataFilterBundle\Tests\Filter\Doctrine\NotInstanceOfFilterTest',
                    ]
                ]
            ],
        ];
    }

    protected function getDataType($value)
    {
        return StringDataType::class;
    }
}
