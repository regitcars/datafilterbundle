<?php

namespace Soluti\DataFilterBundle\Tests\Filter;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\DataType\StringDataType;
use Soluti\DataFilterBundle\Exception\InvalidFilterException;
use Soluti\DataFilterBundle\Filter\Doctrine\ExactFilter;
use Soluti\DataFilterBundle\Filter\Doctrine\LikeFilter;
use Soluti\DataFilterBundle\Filter\FilterConfiguration;
use Symfony\Component\HttpFoundation\Request;

class FilterConfigurationTest extends TestCase
{
    public function testInvalidFilter()
    {
        $this->expectException(\Exception::class);
        new FilterConfiguration(
            [
                new Request(),
            ]
        );
    }

    public function testDuplicateNameFilter()
    {
        $this->expectException(InvalidFilterException::class);
        $filterConfiguration = new FilterConfiguration(
            [
                new LikeFilter('first_name', StringDataType::class, 'a.alias', ['index' => 1], 'abc'),
                new ExactFilter('first_name', StringDataType::class, 'a.alias', ['index' => 4], 'def'),
            ]
        );

        $filterConfiguration->getFilterByName('first_name');
    }

    public function testDuplicateIndexFilter()
    {
        $this->expectException(InvalidFilterException::class);
        $filterConfiguration = new FilterConfiguration(
            [
                new LikeFilter('first_name', StringDataType::class, 'a.alias', ['index' => 1], 'abc'),
                new ExactFilter('last_name', StringDataType::class, 'a.alias', ['index' => 1], 'def'),
            ]
        );

        $filterConfiguration->getFilterByIndex(1);
    }

    public function testInvalidIndexFilter()
    {
        $this->expectException(InvalidFilterException::class);
        $filterConfiguration = new FilterConfiguration(
            [
                new LikeFilter('first_name', StringDataType::class, 'a.alias', ['index' => 'n'], 'abc'),
            ]
        );

        $filterConfiguration->getFilterByIndex(1);
    }

    public function testNoIndexFilter()
    {
        $this->expectException(InvalidFilterException::class);
        $filterConfiguration = new FilterConfiguration(
            [
                new LikeFilter('first_name', StringDataType::class, 'a.alias', [], 'abc'),
            ]
        );

        $filterConfiguration->getFilterByIndex(1);
    }

    public function testGetFilterByName()
    {
        $filterConfiguration = $this->getFilterConfiguration();
        $this->assertInstanceOf(LikeFilter::class, $filterConfiguration->getFilterByName('first_name'));
        $this->assertNull($filterConfiguration->getFilterByName('email'));
    }

    public function testGetFilterByIndex()
    {
        $filterConfiguration = $this->getFilterConfiguration();
        $this->assertInstanceOf(ExactFilter::class, $filterConfiguration->getFilterByIndex(4));
        $this->assertNull($filterConfiguration->getFilterByIndex(99));
    }

    public function testGetAllFilters()
    {
        $filterConfiguration = $this->getFilterConfiguration();
        $this->assertCount(2, $filterConfiguration->getAllFilters());
    }

    protected function getFilterConfiguration()
    {
        return new FilterConfiguration(
            [
                new LikeFilter('first_name', StringDataType::class, 'a.alias', ['index' => 1], 'abc'),
                new ExactFilter('last_name', StringDataType::class, 'a.alias', ['index' => 4], 'def'),
            ]
        );
    }
}
