<?php

namespace Soluti\DataFilterBundle\Tests\Filter;

use Soluti\DataFilterBundle\DataType\StringDataType;
use Soluti\DataFilterBundle\Filter\Doctrine\ExactFilter;
use Symfony\Component\HttpFoundation\Request;

class AbstractFilterTest extends BaseFilterTest
{
    public function testGetFilterNoAlias()
    {
        $filterArguments = $this->getFilterArguments('value');
        $filterArguments[2] = null; // reset column name
        $filter = new ExactFilter(...$filterArguments);
        $filterResult = $filter->getFilter('value');
        $this->assertEquals(
            [
                'name' => [
                    'statement' => 'name = :name',
                    'parameters' => ['name' => 'value']
                ]
            ],
            $filterResult
        );
    }

    public function testGetFilterStringDataType()
    {
        $filterArguments = $this->getFilterArguments();
        $filterArguments[1] = StringDataType::class;
        $filter = new ExactFilter(...$filterArguments);
        $filterResult = $filter->getFilter('value');
        $this->assertEquals(
            [
            'name' => [
                'statement' => 'a.alias = :name',
                'parameters' => ['name' => 'value']
            ]
            ],
            $filterResult
        );
    }

    public function testGetFilterWrongDataType()
    {
        $this->expectExceptionMessage('Unknown Data Type');
        $filterArguments = $this->getFilterArguments();
        $filterArguments[1] = Request::class;
        $filter = new ExactFilter(...$filterArguments);
        $filterResult = $filter->getFilter('value');
        $this->assertEquals(
            [
            'name' => [
                'statement' => 'a.alias = :name',
                'parameters' => ['name' => 'value']
            ]
            ],
            $filterResult
        );
    }

    public function testGetFilterDefault()
    {
        $filter = new ExactFilter(...$this->getFilterArguments('default'));
        $filterResult = $filter->getDefaultFilter();
        $this->assertEquals(
            [
                'name' => [
                    'statement' => 'a.alias = :name',
                    'parameters' => ['name' => 'default']
                ],
            ],
            $filterResult
        );
    }

    public function testGetName()
    {
        $filter = new ExactFilter(...$this->getFilterArguments('default'));
        $this->assertEquals(
            'name',
            $filter->getName()
        );
    }

    public function testGetIndex()
    {
        $arguments = $this->getFilterArguments('default');
        $filter = new ExactFilter(...$arguments);
        $this->assertEquals(5, $filter->getIndex());

        $arguments[3] = [];
        $filter = new ExactFilter(...$arguments);
        $this->assertEquals(null, $filter->getIndex());
    }
}
