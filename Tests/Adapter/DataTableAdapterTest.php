<?php

namespace Soluti\DataFilterBundle\Tests\Adapter;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\Adapter\DataTableAdapter;
use Soluti\DataFilterBundle\DataType\StringDataType;
use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Filter\FilterConfiguration;
use Soluti\DataFilterBundle\Filter\FilterResult;
use Soluti\DataFilterBundle\Filter\Mongo\ExactFilter;
use Soluti\DataFilterBundle\Formatter\FormatterInterface;
use Soluti\DataFilterBundle\Pagination\PaginationConfiguration;
use Soluti\DataFilterBundle\Repository\FilterableRepositoryInterface;
use Soluti\DataFilterBundle\Sort\SortConfiguration;
use Soluti\DataFilterBundle\Sort\SortDefinition;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;
use Symfony\Component\HttpFoundation\Request;
use Traversable;

class DataTableAdapterTest extends TestCase
{
    public function testCollectionFilterEmpty()
    {
        $adapter = new DataTableAdapter($this->getFormatter());
        $collectionFilter = $adapter->getFilter($this->getDefinition(), new Request());
        $this->assertInstanceOf(CollectionFilter::class, $collectionFilter);
        $this->assertEquals(
            [
                'a.first_name' => 'Bob',
                'a.email' => 'bob@test.com',
            ],
            $collectionFilter->getCriteria()
        );
        $this->assertEquals(
            [
                'a.email' => 'bob@test.com',
            ],
            $collectionFilter->getPredefinedCriteria()
        );
        $this->assertEquals(
            [
                'a.first_name' => 'asc',
            ],
            $collectionFilter->getSortOrder()
        );
    }

    public function testCollectionFilterDefaultOverride()
    {
        $adapter = new DataTableAdapter($this->getFormatter());
        $collectionFilter = $adapter->getFilter($this->getDefinition(), new Request([], [
            'filter' => [
                ['column' => '1', 'value' => 'john']
            ]
        ]));
        $this->assertEquals(
            [
                'a.first_name' => 'john',
                'a.email' => 'bob@test.com',
            ],
            $collectionFilter->getCriteria()
        );
    }

    public function testCollectionFilterSort()
    {
        $adapter = new DataTableAdapter($this->getFormatter());
        $collectionFilter = $adapter->getFilter($this->getDefinition(), new Request([], [
            'order' => [
                ['column' => '0', 'dir' => 'desc']
            ]
        ]));
        $this->assertEquals(
            [
                'a.first_name' => 'desc',
            ],
            $collectionFilter->getSortOrder()
        );
    }

    public function testCollectionFilterPredefinedOverride()
    {
        $adapter = new DataTableAdapter($this->getFormatter());
        $collectionFilter = $adapter->getFilter($this->getDefinition(), new Request([], [
            'filter' => [
                ['value' => ''],
                ['value' => ''],
                ['value' => 'john@test.com'],
            ]
        ]));
        $this->assertEquals(
            [
                'a.first_name' => 'Bob',
                'a.email' => 'bob@test.com',
            ],
            $collectionFilter->getCriteria()
        );
    }

    protected function getDefinition()
    {
        return new class implements FilterDefinitionInterface
        {
            public function getPaginationConfiguration(): ?PaginationConfiguration
            {
                return new PaginationConfiguration(25, 100);
            }

            public function getFilterConfiguration(): FilterConfiguration
            {
                return new FilterConfiguration([
                    new ExactFilter('first_name', StringDataType::class, 'a.first_name', ['index' => 0]),
                    new ExactFilter('last_name', StringDataType::class, 'a.last_name', ['index' => 1]),
                    new ExactFilter('email', StringDataType::class, 'a.email', ['index' => 2]),
                ]);
            }

            public function getSortConfiguration(): SortConfiguration
            {
                return new SortConfiguration([
                    new SortDefinition('first_name', 'a.first_name', 0, SortDefinition::SORT_ASC),
                ]);
            }

            public function getDefaultFilterConfiguration(Request $request): FilterConfiguration
            {
                return new FilterConfiguration([
                    new ExactFilter('first_name', StringDataType::class, 'a.first_name', ['index' => 0], 'Bob'),
                ]);
            }

            public function getPredefinedFilterConfiguration(Request $request): FilterConfiguration
            {
                return new FilterConfiguration([
                    new ExactFilter('email', StringDataType::class, 'a.email', ['index' => 2], 'bob@test.com'),
                ]);
            }

            public function getRepositoryService(): FilterableRepositoryInterface
            {
                return new class implements FilterableRepositoryInterface {

                    /**
                     * @param CollectionFilter $filter
                     *
                     * @return FilterResult
                     */
                    public function findFiltered(CollectionFilter $filter)
                    {
                        return new FilterResult($filter, 5, 5, []);
                    }
                };
            }

            public function getTransformerService(): TransformerInterface
            {
                return new class implements TransformerInterface{

                    /**
                     * @param array|Traversable $data
                     *
                     * @return array
                     */
                    public function transformCollection($data)
                    {
                        return [];
                    }

                    /**
                     * @param mixed $data
                     *
                     * @return array
                     */
                    public function transform($data)
                    {
                        return [];
                    }
                };
            }
        };
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject | FormatterInterface
     */
    protected function getFormatter()
    {
        return $this->createMock(FormatterInterface::class);
    }
}
