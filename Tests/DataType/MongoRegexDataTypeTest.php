<?php

namespace DataType;

use MongoDB\BSON\Regex;
use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\DataType\MongoRegexDataType;

class MongoRegexDataTypeTest extends TestCase
{
    public function testPrepare()
    {
        $dataType = new MongoRegexDataType('^fo%so$', 'im');
        $this->assertEquals(new Regex('^foca\$ho$', 'im'), $dataType->prepare('ca$h'));
    }
}
