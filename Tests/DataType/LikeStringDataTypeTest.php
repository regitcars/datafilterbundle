<?php

namespace DataType;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\DataType\LikeStringDataType;

class LikeStringDataTypeTest extends TestCase
{
    public function testPrepareContains()
    {
        $dataType = new LikeStringDataType(LikeStringDataType::CONTAINS);
        $this->assertEquals('%some!_word and 50!% haha%', $dataType->prepare('some_word and 50% haha'));
    }

    public function testPrepareStart()
    {
        $dataType = new LikeStringDataType(LikeStringDataType::STARTS_WITH);
        $this->assertEquals('some!_word and 50!% haha%', $dataType->prepare('some_word and 50% haha'));
    }

    public function testPrepareEnd()
    {
        $dataType = new LikeStringDataType(LikeStringDataType::ENDS_WITH);
        $this->assertEquals('%some!_word and 50!% haha', $dataType->prepare('some_word and 50% haha'));
    }
}
