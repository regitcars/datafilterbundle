<?php

namespace DataType;

use MongoDB\BSON\ObjectID;
use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\DataType\MongoIdDataType;

class MongoIdDataTypeTest extends TestCase
{
    public function testPrepare()
    {
        $dataType = new MongoIdDataType();
        $this->assertEquals(new ObjectID('59a91657a3fa0577c51e6e83'), $dataType->prepare('59a91657a3fa0577c51e6e83'));
    }

    public function testPrepareInvalid()
    {
        $this->expectException(\Exception::class);
        $dataType = new MongoIdDataType();
        $dataType->prepare('123');
    }
}
