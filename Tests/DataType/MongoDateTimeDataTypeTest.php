<?php

namespace DataType;

use MongoDB\BSON\UTCDateTime;
use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\DataType\MongoDateTimeDataType;

class MongoDateTimeDataTypeTest extends TestCase
{
    public function testPrepareDate()
    {
        $dataType = new MongoDateTimeDataType('m-Y-d');
        $this->assertEquals(new UTCDateTime('1484438400000'), $dataType->prepare('01-2017-15'));
    }

    public function testPrepareDateTime()
    {
        $dataType = new MongoDateTimeDataType('m-Y-d', 'H:i:s');
        $this->assertEquals(new UTCDateTime('1484493609000'), $dataType->prepare('01-2017-15 15:20:09'));
    }

    public function testPrepareInvalid()
    {
        $dataType = new MongoDateTimeDataType('m-Y-d', 'H:i:s');
        $this->assertEquals(null, $dataType->prepare('some string'));
    }
}
