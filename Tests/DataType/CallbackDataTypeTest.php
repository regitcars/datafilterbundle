<?php

namespace DataType;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\DataType\CallbackDataType;

class CallbackDataTypeTest extends TestCase
{
    public function testPrepare()
    {
        $dataType = new CallbackDataType(function ($item) {
            return 1 === $item;
        });
        $this->assertEquals(true, $dataType->prepare(1));
        $this->assertEquals(false, $dataType->prepare('1'));
    }
}
