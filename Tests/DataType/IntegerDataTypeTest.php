<?php

namespace DataType;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\DataType\IntegerDataType;

class IntegerDataTypeTest extends TestCase
{
    public function testPrepare()
    {
        $dataType = new IntegerDataType();
        $this->assertTrue(is_int($dataType->prepare('1.1')));
    }
}
