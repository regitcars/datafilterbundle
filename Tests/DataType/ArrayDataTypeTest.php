<?php

namespace DataType;

use Soluti\DataFilterBundle\DataType\ArrayDataType;
use PHPUnit\Framework\TestCase;

class ArrayDataTypeTest extends TestCase
{
    public function testPrepare()
    {
        $dataType = new ArrayDataType();
        $this->assertEquals(['1', 2, 'c'], $dataType->prepare(['1', 2, 'c']));
        $this->assertEquals(['0'], $dataType->prepare('0'));
    }
}
