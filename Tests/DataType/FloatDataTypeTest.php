<?php

namespace DataType;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\DataType\FloatDataType;

class FloatDataTypeTest extends TestCase
{
    public function testPrepare()
    {
        $dataType = new FloatDataType();
        $this->assertTrue(is_float($dataType->prepare('1.1')));
    }
}
