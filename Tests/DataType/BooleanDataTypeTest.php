<?php

namespace DataType;

use Soluti\DataFilterBundle\DataType\BooleanDataType;
use PHPUnit\Framework\TestCase;

class BooleanDataTypeTest extends TestCase
{
    public function testPrepare()
    {
        $dataType = new BooleanDataType();
        $this->assertEquals(true, $dataType->prepare('1'));
        $this->assertEquals(false, $dataType->prepare('0'));
    }
}
