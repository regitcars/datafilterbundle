<?php

namespace Soluti\DataFilterBundle\Tests\Transformer;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\Transformer\AbstractTransformer;

class AbstractTransformerTest extends TestCase
{
    public function testTransform()
    {
        $result = $this->getTransformer()->transform($this->getObject(1));

        $this->assertEquals(
            [
                'first_name' => 'FirstName1',
            ],
            $result
        );
    }

    protected function getTransformer()
    {
        return new class() extends AbstractTransformer
        {
            public function transform($data)
            {
                return [
                    'first_name' => $data->getFirstName(),
                ];
            }
        };
    }

    protected function getObject($i)
    {
        return new class($i)
        {
            protected $firstName;

            public function __construct($i)
            {
                $this->setFirstName('FirstName'.$i);
            }

            public function setFirstName($firstName)
            {
                $this->firstName = $firstName;
            }

            public function getFirstName()
            {
                return $this->firstName;
            }
        };
    }

    public function testTransformCollection()
    {
        $result = $this->getTransformer()->transformCollection(
            [
                $this->getObject(10),
                $this->getObject(55),
            ]
        );

        $this->assertEquals(
            [
                [
                    'first_name' => 'FirstName10',
                ],
                [
                    'first_name' => 'FirstName55',
                ],
            ],
            $result
        );
    }
}
