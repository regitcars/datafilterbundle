<?php

namespace Soluti\DataFilterBundle\Tests\Transformer;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\Manager\FractalManager;
use Soluti\DataFilterBundle\Transformer\ApiAbstractTransformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ApiAbstractTransformerTest extends TestCase
{
    public function testTransform()
    {
        $result = $this->getTransformer()->transform($this->getObject(1));

        $this->assertEquals(
            [
                'first_name' => 'FirstName1',
            ],
            $result
        );
    }

    protected function getTransformer()
    {
        $requestStack = new RequestStack();
        $requestStack->push(new Request());
        $fractalManager = new FractalManager($requestStack);

        return new class($fractalManager) extends ApiAbstractTransformer
        {
            public function transform($data)
            {
                return [
                    'first_name' => $data->getFirstName(),
                ];
            }
        };
    }

    protected function getObject($i)
    {
        return new class($i)
        {
            protected $firstName;

            public function __construct($i)
            {
                $this->setFirstName('FirstName'.$i);
            }

            public function setFirstName($firstName)
            {
                $this->firstName = $firstName;
            }

            public function getFirstName()
            {
                return $this->firstName;
            }
        };
    }

    public function testTransformCollection()
    {
        $result = $this->getTransformer()->transformCollection(
            [
                $this->getObject(10),
                $this->getObject(55),
            ]
        );

        $this->assertEquals(
            [
                'data' => [
                    [
                        'first_name' => 'FirstName10',
                    ],
                    [
                        'first_name' => 'FirstName55',
                    ],
                ],
            ],
            $result
        );
    }
}
