<?php

namespace Soluti\DataFilterBundle\Tests\Formatter;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Filter\FilterResult;
use Soluti\DataFilterBundle\Formatter\ApiFilteredFormatter;
use Soluti\DataFilterBundle\Manager\FractalManager;
use Soluti\DataFilterBundle\Pagination\PaginationConfiguration;
use Soluti\DataFilterBundle\Transformer\ApiAbstractTransformer;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ApiFilteredFormatterTest extends TestCase
{
    /**
     * @dataProvider formatsProvider
     *
     * @param $total
     * @param $filtered
     * @param $page
     * @param $offset
     * @param $limit
     * @param $paginated
     */
    public function testFormat($total, $filtered, $page, $offset, $limit, $paginated)
    {
        $formatter = new ApiFilteredFormatter();
        $formatted = $formatter->format(
            $this->getFilterResult($total, $filtered, $offset, $limit, $paginated),
            $this->getTransformer()
        );

        $this->assertArrayHasKey('data', $formatted);
        if ($paginated) {
            $this->assertArrayHasKey('pagination', $formatted);
            $this->assertEquals($filtered, $formatted['pagination']['total']);
            $this->assertEquals($page, $formatted['pagination']['page']);
            $this->assertEquals($limit, $formatted['pagination']['per_page']);
        } else {
            $this->assertArrayNotHasKey('pagination', $formatted);
        }

        $this->assertEquals(
            [
                ['new_field' => 'First Name'],
                ['new_field' => 'First Name 2'],
                ['new_field' => 'First Name 3'],
            ],
            $formatted['data']
        );
    }

    /**
     * @param $total
     * @param $filtered
     * @param $offset
     * @param $limit
     * @param $paginated
     * @return FilterResult
     */
    private function getFilterResult($total, $filtered, $offset, $limit, $paginated)
    {
        return new FilterResult(
            $this->getCollectionFilter($offset, $limit, $paginated),
            $total,
            $filtered,
            [
                ['first_name' => 'First Name'],
                ['first_name' => 'First Name 2'],
                ['first_name' => 'First Name 3'],
            ]
        );
    }

    /**
     * @param $offset
     * @param $limit
     * @param $paginated
     * @return \PHPUnit_Framework_MockObject_MockObject|CollectionFilter
     */
    private function getCollectionFilter($offset, $limit, $paginated)
    {
        $definition = $this->createMock(FilterDefinitionInterface::class);
        $definition->method('getPaginationConfiguration')
            ->willReturn($paginated ? new PaginationConfiguration() : null);

        $collectionFilter = $this->createMock(CollectionFilter::class);
        $collectionFilter->method('getOffset')
            ->willReturn($offset);
        $collectionFilter->method('getLimit')
            ->willReturn($limit);
        $collectionFilter->method('getDefinition')
            ->willReturn($definition);

        return $collectionFilter;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject | TransformerInterface
     */
    private function getTransformer()
    {
        $requestStack = new RequestStack();
        $requestStack->push(new Request());
        $fractalManager = new FractalManager($requestStack);

        $transformer = new class($fractalManager) extends ApiAbstractTransformer
        {
            /**
             * @param mixed $data
             * @return array
             */
            public function transform($data)
            {
                return [
                    'new_field' => $data['first_name'],
                ];
            }
        };


        return $transformer;
    }

    public function formatsProvider()
    {
        return [
            [100, 10, 1, 0, 10, true],
            [100, 11, 2, 10, 10, true],
            [100, 21, 1, 1, 20, true],
        ];
    }
}
