<?php

namespace Soluti\DataFilterBundle\Tests\Formatter;

use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Filter\FilterResult;
use Soluti\DataFilterBundle\Formatter\DataTablesFormatter;
use Soluti\DataFilterBundle\Transformer\AbstractTransformer;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;

class DataTablesFormatterTest extends TestCase
{
    /**
     * @dataProvider formatsProvider
     *
     * @param $total
     * @param $filtered
     * @param $offset
     * @param $limit
     */
    public function testFormat($total, $filtered, $offset, $limit)
    {
        $formatter = new DataTablesFormatter();
        $formatted = $formatter->format(
            $this->getFilterResult($total, $filtered, $offset, $limit),
            $this->getTransformer()
        );

        $this->assertEquals($total, $formatted['recordsTotal']);
        $this->assertEquals($filtered, $formatted['recordsFiltered']);
        $this->assertArrayHasKey('data', $formatted);
        $this->assertEquals(
            [
                ['new_field' => 'First Name'],
                ['new_field' => 'First Name 2'],
                ['new_field' => 'First Name 3'],
            ],
            $formatted['data']
        );
    }

    /**
     * @param $total
     * @param $filtered
     * @param $offset
     * @param $limit
     * @return FilterResult
     */
    private function getFilterResult($total, $filtered, $offset, $limit)
    {
        return new FilterResult(
            $this->getCollectionFilter($offset, $limit),
            $total,
            $filtered,
            [
                ['first_name' => 'First Name'],
                ['first_name' => 'First Name 2'],
                ['first_name' => 'First Name 3'],
            ]
        );
    }

    /**
     * @param $offset
     * @param $limit
     * @return \PHPUnit_Framework_MockObject_MockObject|CollectionFilter
     */
    private function getCollectionFilter($offset, $limit)
    {
        $collectionFilter = $this->createMock(CollectionFilter::class);
        $collectionFilter->method('getOffset')
            ->willReturn($offset);
        $collectionFilter->method('getLimit')
            ->willReturn($limit);

        return $collectionFilter;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject | TransformerInterface
     */
    private function getTransformer()
    {
        $transformer = new class() extends AbstractTransformer
        {
            /**
             * @param mixed $data
             * @return array
             */
            public function transform($data)
            {
                return [
                    'new_field' => $data['first_name'],
                ];
            }
        };


        return $transformer;
    }

    public function formatsProvider()
    {
        return [
            [100, 10, 0, 10],
            [100, 11, 10, 10],
            [100, 21, 1, 20],
        ];
    }
}
