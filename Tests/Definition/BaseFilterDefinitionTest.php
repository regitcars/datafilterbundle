<?php

namespace Definition;

use Soluti\DataFilterBundle\Definition\BaseFilterDefinition;
use PHPUnit\Framework\TestCase;
use Soluti\DataFilterBundle\Filter\FilterConfiguration;
use Soluti\DataFilterBundle\Pagination\PaginationConfiguration;
use Soluti\DataFilterBundle\Repository\FilterableRepositoryInterface;
use Soluti\DataFilterBundle\Sort\SortConfiguration;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;
use Symfony\Component\HttpFoundation\Request;

class BaseFilterDefinitionTest extends TestCase
{
    public function testGetters()
    {
        $repository = $this->createMock(FilterableRepositoryInterface::class);
        $transformer = $this->createMock(TransformerInterface::class);
        $request = new Request();

        $definition = new class($repository, $transformer) extends BaseFilterDefinition{
        };
        $this->assertEquals($repository, $definition->getRepositoryService());
        $this->assertEquals($transformer, $definition->getTransformerService());
        $this->assertEquals(new FilterConfiguration(), $definition->getFilterConfiguration());
        $this->assertEquals(new FilterConfiguration(), $definition->getPredefinedFilterConfiguration($request));
        $this->assertEquals(new FilterConfiguration(), $definition->getDefaultFilterConfiguration($request));
        $this->assertEquals(new PaginationConfiguration(), $definition->getPaginationConfiguration());
        $this->assertEquals(new SortConfiguration(), $definition->getSortConfiguration());
    }
}
