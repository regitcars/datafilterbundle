<?php

namespace Soluti\DataFilterBundle\Filter;

use Traversable;

class FilterResult
{
    /** @var CollectionFilter */
    protected $filter;

    /** @var int */
    protected $totalResults;

    /** @var int */
    protected $filteredResults;

    /** @var array|Traversable */
    protected $data;

    /**
     * FilterResult constructor.
     * @param CollectionFilter $filter
     * @param int $totalResults
     * @param int $filteredResults
     * @param array|Traversable $data
     */
    public function __construct(CollectionFilter $filter, $totalResults, $filteredResults, $data)
    {
        $this->filter = $filter;
        $this->totalResults = $totalResults;
        $this->filteredResults = $filteredResults;
        $this->data = $data;
    }

    /**
     * @return CollectionFilter
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @return int
     */
    public function getTotalResults()
    {
        return $this->totalResults;
    }

    /**
     * @return Traversable
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return bool
     */
    public function hasMore()
    {
        return $this->filter->getOffset() + $this->filter->getLimit() < $this->getFilteredResults();
    }

    /**
     * @return int
     */
    public function getFilteredResults()
    {
        return $this->filteredResults;
    }
}
