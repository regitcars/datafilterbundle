<?php

namespace Soluti\DataFilterBundle\Filter;

use Soluti\DataFilterBundle\DataType\DataTypeInterface;
use Soluti\DataFilterBundle\Exception\InvalidFilterException;

abstract class AbstractFilter implements FilterInterface
{
    /**@var string */
    protected $name;

    /** @var DataTypeInterface */
    protected $dataType;

    /** @var string */
    protected $columnName;

    /** @var array */
    protected $options;

    /** @var mixed */
    private $defaultValue;

    public function __construct(
        string $name,
        $dataType,
        string $columnName = null,
        array $options = [],
        $defaultValue = null
    ) {
        if (null === $columnName) {
            $columnName = $name;
        }

        $this->name = $name;
        $this->dataType = $this->prepareDataType($dataType);
        $this->columnName = $columnName;
        $this->options = $options;
        $this->defaultValue = $defaultValue;
    }

    /**
     * @param string|DataTypeInterface $dataType
     * @return DataTypeInterface
     * @throws \Exception
     */
    private function prepareDataType($dataType)
    {
        if (is_string($dataType) && class_exists($dataType)) {
            $dataType = new $dataType;
        }

        if ($dataType instanceof DataTypeInterface) {
            return $dataType;
        }

        throw new \Exception('Unknown Data Type');
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getIndex(): ?int
    {
        if (!array_key_exists('index', $this->options) || null === $this->options['index']) {
            return null;
        }

        if (!is_int($this->options['index'])) {
            throw new InvalidFilterException('Filter index is not numeric');
        }

        return $this->options['index'];
    }

    public function getDefaultFilter(): array
    {
        return $this->getFilter($this->defaultValue);
    }

    /**
     * Checks if value is not null or empty string.
     * 0, false '0' are considered valid values.
     *
     * @param mixed $value
     * @return bool
     */
    protected function isEmpty($value): bool
    {
        if (is_array($value)) {
            $isEmpty = true;
            foreach ($value as $item) {
                $isEmpty = $isEmpty && $this->isEmpty($item);
            }

            return $isEmpty;
        }

        return is_null($value) || $value === '';
    }
}
