<?php

namespace Soluti\DataFilterBundle\Filter\Doctrine;

use Soluti\DataFilterBundle\Exception\InvalidValueException;
use Soluti\DataFilterBundle\Filter\AbstractFilter;
use Soluti\DataFilterBundle\Filter\FilterInterface;

class NotInstanceOfFilter extends AbstractFilter implements FilterInterface
{
    public function getFilter($value): array
    {
        if ($this->isEmpty($value)) {
            return [];
        }

        $value = $this->dataType->prepare($value);

        if (!$this->isValid($value)) {
            throw new InvalidValueException(
                sprintf('%s could not be found', $value)
            );
        }

        $result[$this->name]['statement'] = sprintf(
            '%s NOT INSTANCE OF %s',
            $this->columnName,
            $value
        );

        return $result;
    }

    /**
     * @param $value
     * @return bool
     */
    private function isValid($value)
    {
        return class_exists($value);
    }
}
