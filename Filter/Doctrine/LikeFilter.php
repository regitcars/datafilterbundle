<?php

namespace Soluti\DataFilterBundle\Filter\Doctrine;

use Soluti\DataFilterBundle\Filter\AbstractFilter;
use Soluti\DataFilterBundle\Filter\FilterInterface;

class LikeFilter extends AbstractFilter implements FilterInterface
{
    public function getFilter($value): array
    {
        if ($this->isEmpty($value)) {
            return [];
        }

        $result[$this->name]['statement'] = sprintf("%s LIKE :%s ESCAPE '!'", $this->columnName, $this->name);
        $result[$this->name]['parameters'][$this->name] = $this->dataType->prepare($value);

        return $result;
    }
}
