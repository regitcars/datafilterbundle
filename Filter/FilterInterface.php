<?php

namespace Soluti\DataFilterBundle\Filter;

interface FilterInterface
{
    /**
     * @param string $name - name of the filter
     * @param mixed $dataType - class name or instance of ValueTypeInterface
     * @param string|null $columnName -
     * @param array $options
     * @param mixed $defaultValue - the value for default or predefined filters
     */
    public function __construct(
        string $name,
        $dataType,
        string $columnName = null,
        array $options = [],
        $defaultValue = null
    );

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return int|null
     */
    public function getIndex(): ?int;

    /**
     * @param mixed $value
     * @return mixed
     */
    public function getFilter($value): array;

    /**
     * @return array
     */
    public function getDefaultFilter(): array;
}
