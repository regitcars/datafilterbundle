<?php

namespace Soluti\DataFilterBundle\Filter;

use Soluti\DataFilterBundle\Exception\InvalidFilterException;

class FilterConfiguration
{
    /** @var array|FilterInterface[] */
    private $filters;

    /** @var array|FilterInterface[] */
    private $filtersByName;

    /** @var array|FilterInterface[] */
    private $filtersByIndex;

    /**
     * @param array $filters
     * @throws \Exception
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $this->validateFilters($filters);
    }

    /**
     * @param array $filters
     * @return array
     * @throws \Exception
     */
    private function validateFilters(array $filters): array
    {
        foreach ($filters as $filter) {
            if (!$filter instanceof FilterInterface) {
                throw new InvalidFilterException('Provided filter does not implement FilterInterface');
            }
        }

        return $filters;
    }

    /**
     * @return array
     */
    public function getAllFilters(): array
    {
        $criteria = [];
        foreach ($this->filters as $filter) {
            $criteria = array_merge($criteria, $filter->getDefaultFilter());
        }

        return $criteria;
    }

    public function getFilterByName(string $name): ?FilterInterface
    {
        if (null === $this->filtersByName) {
            $this->initNameIndex();
        }

        return $this->filtersByName[$name] ?? null;
    }

    private function initNameIndex(): void
    {
        if (null === $this->filtersByName) {
            $this->filtersByName = [];
        }

        foreach ($this->filters as $filter) {
            if (array_key_exists(
                $filter->getName(),
                $this->filtersByName
            )) {
                throw new InvalidFilterException('Filter with same name defined twice');
            }

            $this->filtersByName[$filter->getName()] = $filter;
        }
    }

    public function getFilterByIndex(int $index): ?FilterInterface
    {
        if (null === $this->filtersByIndex) {
            $this->initNumericIndex();
        }

        return $this->filtersByIndex[$index] ?? null;
    }

    private function initNumericIndex(): void
    {
        foreach ($this->filters as $filter) {
            if (null === $filter->getIndex()) {
                throw new InvalidFilterException('Filter index is not defined');
            }

            if (isset($this->filtersByIndex[$filter->getIndex()])) {
                throw new InvalidFilterException('Filter with same index defined twice');
            }

            $this->filtersByIndex[$filter->getIndex()] = $filter;
        }
    }
}
