<?php

namespace Soluti\DataFilterBundle\Filter\Mongo;

use Soluti\DataFilterBundle\Filter\AbstractFilter;
use Soluti\DataFilterBundle\Filter\FilterInterface;

class RangeFilter extends AbstractFilter implements FilterInterface
{
    public function getFilter($value): array
    {
        if ($this->isEmpty($value)) {
            return [];
        }

        $range = [];
        if (array_key_exists('start', $value) && !$this->isEmpty($value['start'])) {
            $start = $this->dataType->prepare($value['start']);
            if (!$this->isEmpty($start)) {
                $range['$gte'] = $start;
            }
        }

        if (array_key_exists('end', $value) && !$this->isEmpty($value['end'])) {
            $end = $this->dataType->prepare($value['end']);
            if (!$this->isEmpty($end)) {
                $range['$lte'] = $end;
            }
        }

        return [
            $this->columnName => $range,
        ];
    }
}
