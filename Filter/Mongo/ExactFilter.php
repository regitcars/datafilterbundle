<?php

namespace Soluti\DataFilterBundle\Filter\Mongo;

use Soluti\DataFilterBundle\Filter\AbstractFilter;
use Soluti\DataFilterBundle\Filter\FilterInterface;

class ExactFilter extends AbstractFilter implements FilterInterface
{
    public function getFilter($value): array
    {
        if ($this->isEmpty($value)) {
            return [];
        }

        return [
            $this->columnName => $this->dataType->prepare($value),
        ];
    }
}
