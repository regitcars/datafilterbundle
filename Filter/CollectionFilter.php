<?php

namespace Soluti\DataFilterBundle\Filter;

use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;

class CollectionFilter
{
    /** @var FilterDefinitionInterface */
    protected $definition;

    /** @var array */
    protected $criteria;

    /** @var array */
    protected $predefinedCriteria;

    /** @var array */
    protected $sortOrder;

    /** @var int */
    protected $offset;

    /** @var int */
    protected $limit;

    /**
     * @param FilterDefinitionInterface $definition
     */
    public function __construct(FilterDefinitionInterface $definition)
    {
        $this->definition = $definition;
        $this->criteria = [];
        $this->predefinedCriteria = [];
    }

    /**
     * @return FilterDefinitionInterface
     */
    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * @return array
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @param array $criteria
     */
    public function setCriteria($criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * @return array
     */
    public function getPredefinedCriteria()
    {
        return $this->predefinedCriteria;
    }

    /**
     * @param array $predefinedCriteria
     */
    public function setPredefinedCriteria($predefinedCriteria)
    {
        $this->predefinedCriteria = $predefinedCriteria;
    }

    /**
     * @return integer
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return integer
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return array
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param array $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }
}
