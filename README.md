Soluti DataFilterBundle
=======================

Data filtering and pagination made easy.

The implementation is based on:

 * having adapters for Input filter format. Contains 2 default implementations (DataTables and Api)
 * formatters for the output format (DataTables, Api, Raw)
 * adapters for the underlying DB (MongoDB and Doctrine ORM)
 
Every aspect can be extended as you see fit either by replacing the implementation or implementing the interfaces.

## Installation

Require the `soluti/data-filter-bundle` package in your composer.json and update your dependencies.

    $ composer require soluti/data-filter-bundle

Add the SolutiDataFilterBundle to your application's kernel:

```php
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new Soluti\DataFilterBundle\SolutiDataFilterBundle(),
            // ...
        );
        // ...
    }
```

## Creating your first filter

The bundle comes with 2 predefined input adapters:

* DataTables Adapter (`Soluti\DataFilterBundle\Adapter\DataTableAdapter`): for parsing requests from DataTables
* Api Adapter (`Soluti\DataFilterBundle\Adapter\ApiAdapter`): for Api requests

In order to get it working we need to do some setup first. 3 things are required:

* a filter definition
* a transformer 
* a repository service

Each filter definition implements ```Soluti\DataFilterBundle\Definition\FilterDefinitionInterface```, you can also use
```Soluti\DataFilterBundle\Definition\BaseFilterDefinition``` as a starting point. The base class expects 2 parameters:

* the repository service that implements `Soluti\DataFilterBundle\Repository\FilterableRepositoryInterface` there are 2 default implementations:

     * `Soluti\DataFilterBundle\Repository\DoctrineORMRepository`
     * `Soluti\DataFilterBundle\Repository\MongoRepository`
  
* the transformer that implements ```Soluti\DataFilterBundle\Transformer\TransformerInterface```, normally you should extend:

     * for Api ```Soluti\DataFilterBundle\Transformer\ApiAbstractTransformer```
     * for DataTables ```Soluti\DataFilterBundle\Transformer\AbstractTransformer```

Normally both the transformer and the Filter Definition should be registered as services inside the app.

After that inside the controller it is as simple as:

```php
use Soluti\DataFilterBundle\Adapter\DataTableAdapter;
use App\Filter\Definition\UserDefinition;
...
        return new JsonResponse(
            $this
                ->get(DataTableAdapter::class)
                ->process($this->get(UserDefinition::class), $request)
        );
...
```

**Hint:** you can inject your services as method parameters instead of getting them from the Controller.

Please check the following files for examples:

[Definition example](doc/example_definition.md)

[Transformer example](doc/example_transformers.md)

## License

This package is available under the [MIT license](LICENSE).
