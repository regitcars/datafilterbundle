<?php

namespace Soluti\DataFilterBundle\DataType;

class IntegerDataType implements DataTypeInterface
{
    /**
     * @inheritdoc
     */
    public function prepare($value)
    {
        return (int)$value;
    }
}
