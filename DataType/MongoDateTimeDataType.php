<?php

namespace Soluti\DataFilterBundle\DataType;

use DateTime;
use MongoDB\BSON\UTCDateTime;

class MongoDateTimeDataType implements DataTypeInterface
{
    /** @var string */
    private $formatDate;

    /** @var string */
    private $formatTime;

    /**
     * @param string $formatDate
     * @param string $formatTime
     */
    public function __construct($formatDate = 'd-m-Y', $formatTime = null)
    {
        $this->formatDate = $formatDate;
        $this->formatTime = $formatTime;
    }

    /**
     * @inheritdoc
     */
    public function prepare($value)
    {
        $format = $this->formatDate;
        if ($this->formatTime) {
            $format .= ' '.$this->formatTime;
        }
        $timezone = new \DateTimeZone('UTC');
        $parsedDate = DateTime::createFromFormat($format, $value, $timezone);
        if ($parsedDate) {
            if (!$this->formatTime) {
                $parsedDate->setTime(0, 0, 0);
            }

            return new UTCDatetime($parsedDate->getTimestamp().'000');
        }

        return null;
    }
}
