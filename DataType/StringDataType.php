<?php

namespace Soluti\DataFilterBundle\DataType;

class StringDataType implements DataTypeInterface
{
    /**
     * @inheritdoc
     */
    public function prepare($value)
    {
        return (string)$value;
    }
}
