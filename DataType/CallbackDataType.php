<?php

namespace Soluti\DataFilterBundle\DataType;

class CallbackDataType implements DataTypeInterface
{
    /** @var callable */
    private $prepareFunction;

    /**
     * @param callable $prepareFunction
     */
    public function __construct(callable $prepareFunction)
    {
        $this->prepareFunction = $prepareFunction;
    }

    /**
     * @inheritdoc
     */
    public function prepare($value)
    {
        return ($this->prepareFunction)($value);
    }
}
