<?php

namespace Soluti\DataFilterBundle\DataType;

use MongoDB\BSON\ObjectID;

class MongoIdDataType implements DataTypeInterface
{
    /**
     * @inheritdoc
     */
    public function prepare($value)
    {
        return new ObjectID($value);
    }
}
