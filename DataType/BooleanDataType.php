<?php

namespace Soluti\DataFilterBundle\DataType;

class BooleanDataType implements DataTypeInterface
{
    /**
     * @inheritdoc
     */
    public function prepare($value)
    {
        return (bool)$value;
    }
}
