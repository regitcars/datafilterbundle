<?php

namespace Soluti\DataFilterBundle\DataType;

class ArrayDataType implements DataTypeInterface
{
    /**
     * @inheritdoc
     */
    public function prepare($value)
    {
        return (array)$value;
    }
}
