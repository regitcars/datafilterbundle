<?php

namespace Soluti\DataFilterBundle\DataType;

interface DataTypeInterface
{
    /**
     * Prepare a raw value to a expected one
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepare($value);
}
