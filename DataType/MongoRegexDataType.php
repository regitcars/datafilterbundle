<?php

namespace Soluti\DataFilterBundle\DataType;

use MongoDB\BSON\Regex;

class MongoRegexDataType implements DataTypeInterface
{
    /** @var string */
    private $pattern;

    /** @var string */
    private $flags;

    /**
     * @param $pattern
     * @param $flags
     */
    public function __construct(string $pattern, string $flags = 'i')
    {
        $this->pattern = $pattern;
        $this->flags = $flags;
    }

    /**
     * @inheritdoc
     */
    public function prepare($value)
    {
        return new Regex(
            sprintf($this->pattern, preg_quote($value, '/')),
            $this->flags
        );
    }
}
