<?php

namespace Soluti\DataFilterBundle\DataType;

class FloatDataType implements DataTypeInterface
{
    /**
     * @inheritdoc
     */
    public function prepare($value)
    {
        return (float)$value;
    }
}
