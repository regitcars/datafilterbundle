<?php

namespace Soluti\DataFilterBundle\Adapter;

use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Formatter\FormatterInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class BaseAdapter
{
    /**
     * @var FormatterInterface
     */
    protected $formatter;

    /**
     * @param FormatterInterface $formatter
     */
    public function __construct(FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * Builds a Collection filter based on definition and Request
     *
     * @param string|FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter|null $collectionFilter
     *
     * @return array
     *
     * @throws \Exception
     */
    public function process($definition, Request $request, CollectionFilter $collectionFilter = null)
    {
        $definition = $this->initializeDefinition($definition);
        $filter = $this->getFilter($definition, $request, $collectionFilter);
        $data = $definition->getRepositoryService()->findFiltered($filter);

        return $this->formatter->format($data, $definition->getTransformerService());
    }

    /**
     * @param $definition
     * @return FilterDefinitionInterface
     * @throws \Exception
     */
    private function initializeDefinition($definition): FilterDefinitionInterface
    {
        if ($definition instanceof FilterDefinitionInterface) {
            return $definition;
        }

        if (!in_array(FilterDefinitionInterface::class, class_implements($definition))) {
            throw new \Exception(
                sprintf('%s does not implement FilterDefinitionInterface interface.', $definition)
            );
        }

        return new $definition;
    }

    /**
     * @param FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter|null $collectionFilter
     *
     * @return CollectionFilter
     */
    public function getFilter(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter = null
    ) {
        if (is_null($collectionFilter)) {
            $collectionFilter = new CollectionFilter($definition);
        }

        $this->processPagination($definition, $request, $collectionFilter);
        $this->processSortable($definition, $request, $collectionFilter);
        $this->processFilters($definition, $request, $collectionFilter);

        return $collectionFilter;
    }

    /**
     * Prepare query params for pagination
     *
     * @param FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter $collectionFilter
     *
     * @return mixed
     */
    abstract protected function processPagination(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    );

    /**
     * Prepare query params for sortable
     *
     * @param FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter $collectionFilter
     *
     * @return mixed
     */
    abstract protected function processSortable(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    );

    /**
     * Prepare query params for filters
     *
     * @param FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter $collectionFilter
     *
     * @return mixed
     */
    abstract protected function processFilters(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    );
}
