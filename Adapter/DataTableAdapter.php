<?php

namespace Soluti\DataFilterBundle\Adapter;

use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Pagination\PaginationConfiguration;
use Symfony\Component\HttpFoundation\Request;

class DataTableAdapter extends BaseAdapter implements AdapterInterface
{
    protected function processPagination(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        $paginationConfiguration = $definition->getPaginationConfiguration();
        if (!$paginationConfiguration) {
            return;
        }

        list($offset, $limit) = $paginationConfiguration->getByOffset(
            (int)$request->request->get('start', 0),
            (int)$request->request->get('length', PaginationConfiguration::DEFAULT_RESULT_COUNT)
        );

        $collectionFilter->setOffset($offset);
        $collectionFilter->setLimit($limit);
    }

    protected function processSortable(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        $sortConfiguration = $definition->getSortConfiguration();
        $sort = [];
        foreach ($request->request->get('order', []) as $value) {
            $columnIndex = (int)($value['column'] ?? -1);
            $sortDefinition = $sortConfiguration->getSortDefinitionByIndex($columnIndex);
            if ($sortDefinition) {
                $sort = array_merge($sort, $sortDefinition->getSortOrder($value['dir']));
            }
        }

        if (empty($sort)) {
            foreach ($sortConfiguration->getAllDefinitions() as $sortDefinition) {
                $sort = array_merge($sort, $sortDefinition->getDefaultSortOrder());
            }
        }

        $collectionFilter->setSortOrder($sort);
    }

    protected function processFilters(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        $filterConfiguration = $definition->getFilterConfiguration();
        $criteria = [];
        foreach ($request->request->get('filter', []) as $columnIndex => $value) {
            $filter = $filterConfiguration->getFilterByIndex($columnIndex);
            if ($filter) {
                $criteria = array_merge($criteria, $filter->getFilter($value['value']));
            }
        }


        $predefinedFilters = $definition->getPredefinedFilterConfiguration($request)->getAllFilters();
        $collectionFilter->setCriteria(
            array_merge(
                $definition->getDefaultFilterConfiguration($request)->getAllFilters(),
                $criteria,
                $predefinedFilters
            )
        );

        $collectionFilter->setPredefinedCriteria($predefinedFilters);
    }
}
