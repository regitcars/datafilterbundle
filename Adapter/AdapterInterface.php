<?php

namespace Soluti\DataFilterBundle\Adapter;

use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Symfony\Component\HttpFoundation\Request;

interface AdapterInterface
{
    /**
     * Builds a Collection filter based on definition and Request
     *
     * @param string|FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter|null $collectionFilter
     *
     * @return CollectionFilter
     *
     * @throws \Exception
     */
    public function process($definition, Request $request, CollectionFilter $collectionFilter = null);
}
