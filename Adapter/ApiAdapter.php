<?php

namespace Soluti\DataFilterBundle\Adapter;

use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Pagination\PaginationConfiguration;
use Symfony\Component\HttpFoundation\Request;

class ApiAdapter extends BaseAdapter implements AdapterInterface
{
    protected function processPagination(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        $paginationConfiguration = $definition->getPaginationConfiguration();
        if (!$paginationConfiguration) {
            return;
        }

        list($offset, $limit) = $paginationConfiguration->getByPage(
            (int)$request->query->get('page', 1),
            (int)$request->query->get('per_page', PaginationConfiguration::DEFAULT_RESULT_COUNT)
        );

        $collectionFilter->setOffset($offset);
        $collectionFilter->setLimit($limit);
    }

    protected function processSortable(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        $sortConfiguration = $definition->getSortConfiguration();
        $sort = [];
        foreach ($request->query->get('order', []) as $columnName => $value) {
            $sortDefinition = $sortConfiguration->getSortDefinitionByName($columnName);
            if ($sortDefinition) {
                $sort = array_merge($sort, $sortDefinition->getSortOrder($value));
            }
        }

        if (empty($sort)) {
            foreach ($sortConfiguration->getAllDefinitions() as $sortDefinition) {
                $sort = array_merge($sort, $sortDefinition->getDefaultSortOrder());
            }
        }

        $collectionFilter->setSortOrder($sort);
    }

    protected function processFilters(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        $filterConfiguration = $definition->getFilterConfiguration();
        $criteria = [];
        foreach ($request->query->get('filter', []) as $columnName => $value) {
            $filter = $filterConfiguration->getFilterByName($columnName);
            if ($filter) {
                $criteria = array_merge($criteria, $filter->getFilter($value));
            }
        }

        $predefinedFilters = $definition->getPredefinedFilterConfiguration($request)->getAllFilters();
        $collectionFilter->setCriteria(
            array_merge(
                $definition->getDefaultFilterConfiguration($request)->getAllFilters(),
                $criteria,
                $predefinedFilters
            )
        );

        $collectionFilter->setPredefinedCriteria($predefinedFilters);
    }
}
